
import java.util.Random;



enum Poke_bag {
        // hp ,def, atk, lvl
        Pikachu("Pikachu", 45, 49, 49, 60),       //1
        Greninja("Greninja", 72, 67, 95, 49),       //2
        Charizard("Charizard", 78, 78, 84, 50),      //3
        Bayleef("Bayleef", 60, 80, 60, 41),         //4
        Snorlax("Snorlax", 160, 65, 110, 30),       //5
        Incineroar("Incineroar", 95, 90, 115, 53);  //6

        private String Names;
        private int hp;
        private int def;
        private int atk;
        private int lvl;
        
        Poke_bag(String NAMES, int heath, int defence, int Attag, int Level) {
            this.Names = NAMES;
            this.hp = heath;
            this.def = defence;
            this.atk = Attag;
            this.lvl = Level;
        }
        Random ran = new Random();
        public int getHp(){ return (hp + getLvl())*9; }
    
        public int getDef(){ return def * 3 + getLvl(); }
        
        public int getAtk(){ return (atk + getLvl())*5 ; }
    
        public int getLvl() { return lvl; }
        
        public String getNames(){ return Names; }
        @Override
        public String toString(){
            return Names;
        }
        
}



public class Pokebag {

    public Poke_bag pokePlayer;
    
    public int[] poke_stat = new int[5];
    
    

    int[] callPokeStat (int x) {
        
        if(x == 0) {poke_stat[0] = Poke_bag.Pikachu.getHp(); poke_stat[1] = Poke_bag.Pikachu.getDef(); poke_stat[2] = Poke_bag.Pikachu.getAtk(); poke_stat[3] = Poke_bag.Pikachu.getLvl();}
        else if(x == 1) {poke_stat[0] = Poke_bag.Greninja.getHp(); poke_stat[1] = Poke_bag.Greninja.getDef(); poke_stat[2] = Poke_bag.Greninja.getAtk(); poke_stat[3] = Poke_bag.Greninja.getLvl();}
        else if(x == 2) {poke_stat[0] = Poke_bag.Charizard.getHp(); poke_stat[1] = Poke_bag.Charizard.getDef(); poke_stat[2] = Poke_bag.Charizard.getAtk(); poke_stat[3] = Poke_bag.Charizard.getLvl();}
        else if(x == 3) {poke_stat[0] = Poke_bag.Bayleef.getHp(); poke_stat[1] = Poke_bag.Bayleef.getDef(); poke_stat[2] = Poke_bag.Bayleef.getAtk(); poke_stat[3] = Poke_bag.Bayleef.getLvl();}
        else if(x == 4) {poke_stat[0] = Poke_bag.Snorlax.getHp(); poke_stat[1] = Poke_bag.Snorlax.getDef(); poke_stat[2] = Poke_bag.Snorlax.getAtk(); poke_stat[3] = Poke_bag.Snorlax.getLvl();}
        else if(x == 5) {poke_stat[0] = Poke_bag.Incineroar.getHp(); poke_stat[1] = Poke_bag.Incineroar.getDef(); poke_stat[2] = Poke_bag.Incineroar.getAtk(); poke_stat[3] = Poke_bag.Incineroar.getLvl();}
                
        return poke_stat;
    }

  
}