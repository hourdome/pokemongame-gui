

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Font;
import java.awt.event.*;
import java.awt.Color;


public class Running extends JFrame{
    
    public Running(){               
        init();
    }

   

    private void init(){
        
       
        JButton bn1 = new JButton("START");
        JButton bn2 = new JButton("POKE BAG");
        JButton bn3 = new JButton("EXIT");
        
        JLabel l1 = new JLabel("POKEMON GAME");
        JPanel p1 = new JPanel();
       
        setTitle("Pokemon Game");
        
        setPreferredSize(new java.awt.Dimension(800, 500));
        getContentPane().setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        bn1.setFont(new Font("supermarket", 0, 16));
        bn1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                jButton1ActionPerformed(e);
            }
        });
        bn2.setFont(new Font("supermarket", 0, 16));
        bn2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                jButton2ActionPerformed(e);
            }
        });
        bn3.setFont(new Font("supermarket", 0, 16)); 
        bn3.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                jButton3ActionPerformed(e);
            }
        });
        flatIcon(bn1);
        flatIcon(bn2);
        flatIcon(bn3);
        
        l1.setFont(new Font("supermarket", 0, 24));
        //p1.setBackground(new Color(153,153,153));
        p1.setLayout(null);
        p1.add(bn1);
        bn1.setBounds(327, 260, 146, 40);
        
        p1.add(bn2);
        bn2.setBounds(327, 330, 146, 40);

        p1.add(bn3);
        bn3.setBounds(327, 400, 146, 40);

        l1.setIcon(new ImageIcon("pic/menu.gif"));
        p1.add(l1);
        l1.setBounds(0, -1, 800, 500);
        
        getContentPane().add(p1);
        p1.setBounds(0,0,800,500);

       // p1.add(bg);
       // bg.setBounds(0, 0, 800, 500);

        pack();
        
        setLocationRelativeTo(null);
        setResizable(false);
        
    }
    public void flatIcon(JButton jB) {
        jB.setBorderPainted(false);
        jB.setFocusPainted(false);
        jB.setBackground(new Color(240,240,240));
    }
    private void jButton1ActionPerformed(ActionEvent e) {                                         
        //new BattleBox().setVisible(true);
        new ChoiceBox().setVisible(true);
        this.setVisible(false);
    }

    private void jButton2ActionPerformed(ActionEvent e) {                                         
        new PokebagBox().setVisible(true);
    } 

    private void jButton3ActionPerformed(ActionEvent e) {                                         
        System.exit(0);
        
    } 
        
    public static void main(String[] args) {

        java.awt.EventQueue.invokeLater(new Runnable(){
        
            @Override
            public void run() {
                

                Running r = new Running();
                r.setVisible(true);
                
                
            }
        });
    
       
        
    }
}
