import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;
import javax.swing.SwingConstants;

public class RunBox extends JFrame {
    public RunBox() {
        init();
    }

    public void init() {
        runLabel = new JLabel("RUN");
        setPreferredSize(new java.awt.Dimension(500, 500));
        getContentPane().setLayout(null);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        runLabel.setHorizontalAlignment(SwingConstants.CENTER);
        runLabel.setFont(new Font("supermarket", 1, 200));
       
        runLabel.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                jRunMouseClicked(e);
            }
        });
        getContentPane().add(runLabel);
        runLabel.setBounds(-10, 0, 500, 500);
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
    }

    protected void jRunMouseClicked(MouseEvent e) {
        new Running().setVisible(true);
        this.setVisible(false);
    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new RunBox().setVisible(true);
            }
        });
    }
    private JLabel runLabel;
}