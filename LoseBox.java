import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.*;

public class LoseBox extends JFrame {
    public LoseBox() {
        init();
    }

    public void init() {
        loseLabel = new JLabel("LOSE");
        setPreferredSize(new java.awt.Dimension(500, 500));
        getContentPane().setLayout(null);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        loseLabel.setHorizontalAlignment(SwingConstants.CENTER);
        loseLabel.setFont(new Font("supermarket", 1, 200));
        loseLabel.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                jLoseMouseClicked(e);
            }
        });
        getContentPane().add(loseLabel);
        loseLabel.setBounds(-10, 0, 500, 500);
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
    }

    protected void jLoseMouseClicked(MouseEvent e) {
        new Running().setVisible(true);
        this.setVisible(false);
    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new LoseBox().setVisible(true);
            }
        });
    }
    private JLabel loseLabel;
}