import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;


public class Loading extends JFrame{
    public Loading(){
        init();
    }
    
    private void init(){
        
        JLabel bg= new JLabel();
        JLabel l1 = new JLabel("Press Click To Start");

        setPreferredSize(new java.awt.Dimension(800, 500));
        
        getContentPane().setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        l1.setFont(new Font("supermarket", 1, 16));
        l1.setForeground(new Color(143,143,143));
        getContentPane().add(l1);
        l1.setBounds(340, 400, 120, 40);
        
        bg.setIcon(new ImageIcon("pic/pokeball Rotate.gif"));
        bg.addMouseListener(new MouseAdapter(){
            public void mouseClicked(MouseEvent e){
                jBgMouseClicked(e);
            }
        });
        
        getContentPane().add(bg);
        bg.setBounds(0,-50,800,500);
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
    }
    private void jBgMouseClicked(MouseEvent e){
        new Running().setVisible(true);
        this.setVisible(false);
    }
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable(){
        
            @Override
            public void run() {
              
                Loading l = new Loading();
                l.setVisible(true);
                
            }
        });
    }
    
}