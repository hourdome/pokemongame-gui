
public class ChoicePlayer {
    Action ac = new Action();
    Delay d = new Delay();
    BattleBox b = new BattleBox();
    //Pokemon p = new Pokemon();
                                //enemy,player
    public void PAction(int mode,int[] x, int[] y, Poke_bag EName, int maxHP) {
        if(mode == 1) {

        int damageAttack = ac.attacked(y[2], x[1]);
                if(damageAttack > 0) {
                    b.text.setText(EName + " attack "+ damageAttack + " damages");
                    x[0] = x[0] - damageAttack;
                }
                else {
                    b.text.setText(EName + " miss!!!");
                }
                
        }
        else if(mode == 2){
           
            int damageAttack = ac.skill(y[2], x[1]);
                    if(damageAttack > 0) {
                        b.text.setText(EName + " skill "+ damageAttack + " damages");
                        x[0] = x[0] - damageAttack;
                    }
                    else {
                        b.text.setText(EName + " skill miss!!!");
                    }
                    
        }
    
        else if(mode == 3){
            int heal = ac.heal();
            y[0] = y[0] + heal;
            if(y[0] > maxHP){
                y[0] = maxHP;
            }
            b.text.setText(EName+ " use Potion "+ heal +" hp.");
            
        }
    
        else if(mode == 4){
            
            d.delay(3);
            
            b.text.setText(" RUN!!!");
            
            
          
        }
    }
}