import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;

public class WinBox extends JFrame{
    public WinBox(){
        init();
    }
    public void init(){
        winLabel = new JLabel("WIN");
        setPreferredSize(new java.awt.Dimension(500, 500));
        getContentPane().setLayout(null);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        winLabel.setHorizontalAlignment(SwingConstants.CENTER);
        winLabel.setFont(new Font("supermarket", 1, 200));
        winLabel.addMouseListener(new MouseAdapter(){
            public void mouseClicked(MouseEvent e){
                jWinMouseClicked(e);
            }
        });
       
        getContentPane().add(winLabel);
        winLabel.setBounds(-10,0, 500,500);
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
    }
    
    private void jWinMouseClicked(MouseEvent e) {
        
        new Running().setVisible(true);
        this.setVisible(false);
    }
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable(){
            @Override
            public void run() {
                new WinBox().setVisible(true);    
            }
        });
    }
    private JLabel winLabel;
}