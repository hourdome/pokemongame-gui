
public class Delay {
    public void delay(int x){
        System.out.print("Wait Seconds");
        try{
            for(int i = 0; i < x; i++){
                Thread.sleep(1000);
            }
            System.out.println();
        }
        catch(InterruptedException e){
            System.err.println(e.getMessage());
        }
    }
}