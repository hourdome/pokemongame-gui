
public class RandEnemyAction {
    
    Action ac = new Action();
    Delay d = new Delay(); 
    BattleBox b = new BattleBox();
    //Pokemon pe = new Pokemon();
                        // enemy, player
    public void EnAction(int[] x, int[] y, Status e_Poke, int maxHP) {
        if(ac.EnemyAction() == 1) {
            
            int enemyAttack = ac.attacked(x[2], y[1]);
            if(enemyAttack > 0) {
                b.text.setText(e_Poke + " attack "+ enemyAttack + " damages");
                y[0] = y[0] - enemyAttack;
            }
            else {
                b.text.setText(e_Poke + " miss!!!");
            }
            System.out.println();
        }
        else if(ac.EnemyAction() == 2){
            
            int enemyAttack = ac.skill(x[2], y[1]);
            if(enemyAttack > 0) {
                b.text.setText(e_Poke + " Skill "+ enemyAttack + " damages");
                y[0] = y[0] - enemyAttack;
            }
            else {
                b.text.setText(e_Poke + " skill miss!!!");
            }
            
        }
        else if(ac.EnemyAction() == 3){
            
            int heal = ac.heal();
            x[0] = x[0] + heal;
            if(x[0] > maxHP){
                x[0] = maxHP;
            }
            b.text.setText(e_Poke + " use Berry "+ heal +" hp.");
            

        }
        else if(ac.EnemyAction() == 4){
            
            if(ac.escape() >= 50){
                d.delay(5);
                
                b.text.setText(e_Poke + " RUN!!!"); 
                //pe.randMon();  
            }else{
                d.delay(5);
               
                b.text.setText(e_Poke + " RUN FAILED");
            }

        }
    }

}