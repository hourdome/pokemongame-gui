import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;

public class ChoiceBox extends JFrame{

    public ChoiceBox(){
        init();
    }

    private void init(){
        bl = new JButton("BAYLEEF");
        pk = new JButton("PIKACHU");
        sl = new JButton("SNORLAX");
        ic = new JButton("INCINEROAR");
        cz = new JButton("CHARIZARD");
        gn = new JButton("GRENINJA"); 
        setTitle("Choice Pokemon");
        setPreferredSize(new java.awt.Dimension(310, 399));
        getContentPane().setLayout(null);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        bl.setIcon(new ImageIcon("pic/bayleef.png"));
        pk.setIcon(new ImageIcon("pic/pikachu.png"));
        gn.setIcon(new ImageIcon("pic/greninja.png"));
        cz.setIcon(new ImageIcon("pic/charizard.png"));
        sl.setIcon(new ImageIcon("pic/snorlax.png"));
        ic.setIcon(new ImageIcon("pic/Incineroar.png"));
        set(pk,35); set(gn,35); set(cz,24); set(bl,36); set(sl,30); set(ic,35);
        ChoiceForPlay(pk,0);
        ChoiceForPlay(gn,1);
        ChoiceForPlay(cz,2);
        ChoiceForPlay(bl,3);
        ChoiceForPlay(sl,4);
        ChoiceForPlay(ic,5);
        getContentPane().add(pk);
        pk.setBounds(-1, 0, 300, 60);
        getContentPane().add(gn);
        gn.setBounds(-1, 60, 300, 60);
        getContentPane().add(cz);
        cz.setBounds(-1, 120, 300, 60);
        getContentPane().add(bl);
        bl.setBounds(-1, 180, 300, 60);
        getContentPane().add(sl);
        sl.setBounds(-1, 240, 300, 60);
        getContentPane().add(ic);
        ic.setBounds(-1, 300, 300, 60);
        
        pack();
        setLocationRelativeTo(null);
        setResizable(false);  
    }
    public void ChoiceForPlay(JButton b,int x){
        b.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                jButtonActionPerformed(e,x);
            }
        });    
    } 
    public void jButtonActionPerformed(ActionEvent e,int x) {
        BattleBox b = new BattleBox();
        b.playerSetup(x);
        new BattleBox().setVisible(true);
        this.setVisible(false);
    }
    
    public void set(JButton b, int x){
        b.setHorizontalAlignment(SwingConstants.LEFT);
        b.setHorizontalTextPosition(SwingConstants.RIGHT);
        b.setIconTextGap(x);
        b.setBorderPainted(false);
        b.setFocusPainted(false);
        b.setBackground(new Color(240,240,240));
        b.setFont(new Font("supermarket", 0, 16));
    }
    



    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ChoiceBox().setVisible(true);
            }
        });
    }
    private JButton bl,pk,gn,cz,sl,ic;
    
    
}