import java.util.*;
public class Action {
    Random rand = new Random();
    // yourAtk , oppDef
    public int attacked(int x, int y) {
        
    
        int miss = (int)(Math.random()*100);
        //System.out.println("missing: "+ miss);
        if (miss > 25) {
            return (((x / 2) - ((y % 1 * ((int)(Math.random()*20)+1)))));
        } else {
            return 0;
        }

    }

    public int skill(int x, int y) {
        int a;
        a = ((x * 2) - y) / 2;
        if (a < 0) {
            a = a * (-1);
        }
        return a;
    }

    public int heal() {      
        return 20+(int)(Math.random()*(30-20)+20);   
    
    }

    public int escape() {
        return rand.nextInt(100) + 1;
    }

    public int EnemyAction(){
        
        return (int)(Math.random()*4+1);
        
       
    }

}