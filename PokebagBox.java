import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;



import javax.imageio.ImageIO;
import java.awt.Font;
import java.awt.event.*;
import java.io.File;
import java.awt.Color;

public class PokebagBox extends JFrame{
    
    public PokebagBox(){
        
        init();
    }
   
    private void init(){
        
        bn1 = new JButton("BACK");
        bg = new JLabel();
        p1 = new JPanel();
        poke1 = new JPanel();
        poke2 = new JPanel();
        poke3 = new JPanel();
        poke4 = new JPanel();
        poke5 = new JPanel();
        poke6 = new JPanel();
        l1 = new JLabel();
        l2 = new JLabel();
        l3 = new JLabel();
        l4 = new JLabel();
        l5 = new JLabel();
        l6 = new JLabel();
        h = new JLabel();
        a = new JLabel();
        lv = new JLabel();


        setTitle("Poke Bag");

        setPreferredSize(new java.awt.Dimension(800, 500));
        getContentPane().setLayout(null);
    
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        bn1.setFont(new Font("supermarket", 0, 16));
        bn1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                jButton1ActionPerformed(e);
            }
        });
        l1.addMouseListener(new MouseAdapter(){
            public void mouseClicked(MouseEvent e){
                poke1MouseClicked(e);
            }
        });
        bg.setLayout(null);
        poke1.setLayout(null);
        poke2.setLayout(null);
        poke3.setLayout(null);
        poke4.setLayout(null);
        poke5.setLayout(null);
        poke6.setLayout(null);

        autoAdd(poke1,l1, 0);
        autoAdd(poke2,l2, 1);
        autoAdd(poke3,l3, 2);
        autoAdd(poke4,l4, 3);
        autoAdd(poke5,l5, 4);
        autoAdd(poke6,l6, 5);
        
        
        p1.setLayout(new java.awt.GridLayout());
        flatIcon(bn1);
        p1.add(poke1);
        p1.add(poke2);
        p1.add(poke3);
        p1.add(poke4);
        p1.add(poke5);
        p1.add(poke6);       

        bg.add(p1);
        p1.setBounds(20, 20, 740, 390);

        bg.add(bn1);
        bn1.setBounds(20, 425, 73, 23);

        getContentPane().add(bg);
        bg.setBounds(0, 0, 2000, 500);
        pack();

        setLocationRelativeTo(null);
        setResizable(false);
    
    }
    
   
    public void autoAdd(JPanel jp, JLabel l, int x) {
        Poke_bag[] p = Poke_bag.values();
        h = new JLabel("HP : "+p[x].getHp());
        a = new JLabel("ATK : "+p[x].getAtk());
        lv = new JLabel("LVL : "+p[x].getLvl());
        JLabel bg = new JLabel();
        Poke_bag name = p[x];
        l.setFont(new Font("supermarket", 1, 28));
        h.setFont(new Font("supermarket", 1, 18));
        a.setFont(new Font("supermarket", 1, 18));
        lv.setFont(new Font("supermarket", 1, 18));
        l.setForeground(Color.white);
        h.setForeground(Color.white);
        a.setForeground(Color.white);
        lv.setForeground(Color.white);
        l.setVerticalAlignment(SwingConstants.TOP);
        l.setText(transformStringToHtml(p[x].getNames().toUpperCase()));
        bg.setIcon(new ImageIcon("pic/display/" + name + "_show.gif"));
        jp.add(h);
        jp.add(a);
        jp.add(lv);
        jp.add(l);
        jp.add(bg);
        h.setBounds(10,280,100,30);
        a.setBounds(10,310,100,30);
        lv.setBounds(10,340,100,30);
        bg.setBounds(0, 0, 120, 390);
        l.setBounds(10, 10, 50, 250);
    }

    public void updateStat(int x){
        P_stat[0] = x;
        h.setText("HP : " + P_stat[0]);
    }
    private void poke1MouseClicked(MouseEvent e) {

    }

    public String transformStringToHtml(String strToTransform) {
        String ans = "<html>";
        String br = "<br>";
        String[] lettersArr = strToTransform.split("");
        for (String letter : lettersArr) {
            ans += letter + br;
        }
        ans += "</html>";
        return ans;
    }
    public void flatIcon(JButton jB) {
        jB.setBorderPainted(false);
        jB.setFocusPainted(false);
        jB.setBackground(new Color(240,240,240));
    }
    private void jButton1ActionPerformed(ActionEvent e) {                                         
        this.setVisible(false);
    }
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                
                PokebagBox pb = new PokebagBox();
                pb.setVisible(true);
                //pb.setSize(500,500);
            }
        });
    }
private JButton bn1;
private JLabel bg;
private JPanel p1;
private JPanel poke1;
private JPanel poke2;
private JPanel poke3;
private JPanel poke4;
private JPanel poke5;
private JPanel poke6;
private JLabel l1;
private JLabel l2;
private JLabel l3;
private JLabel l4;
private JLabel l5;
private JLabel l6;
private JLabel h;
private JLabel a;
private JLabel lv;

public int[] P_stat = new int[3];
}


        