
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.event.*;
import java.awt.Font;


public class BattleBox extends JFrame{
    /* Screen s = new Screen();
    Choice choice = new Choice(); */
    public BattleBox(){
        randMon();
        init();
    }
    
    public void init(){        
        jPanel1 = new JPanel();
        jPanel2 = new JPanel();
        picPlayer = new JPanel();
        statusPlyer = new JPanel();
        picEnemy = new JPanel();
        statusEnemy = new JPanel();
        player = new JLabel();
        enemy = new JLabel();
        playerHP = new JLabel();
        enemyHP = new JLabel();
        pokePlayer = new JLabel();
        pokeEnemy = new JLabel();
        numHpEnemy = new JLabel();
        numHpPlayer = new JLabel();
        HP_p = new JProgressBar();
        HP_e = new JProgressBar();
        jButton1 = new JButton("ATTACK");
        jButton2 = new JButton("POTION");
        jButton3 = new JButton("SKILL");
        jButton4 = new JButton("RUN"); 
        text = new JTextArea();
        setTitle("Battle");
        setPreferredSize(new java.awt.Dimension(800, 500));
        getContentPane().setLayout(null);

        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        text.setFont(new Font("supermarket", 0, 24));
        text.setBackground(Color.darkGray);
        text.setForeground(Color.white);
        text.setLineWrap(true);
        jButton1.addActionListener(new ActionListener(){ public void actionPerformed(ActionEvent e){ attack(e); }});   
        jButton2.addActionListener(new ActionListener(){ public void actionPerformed(ActionEvent e){ potion(e); }});   
        jButton3.addActionListener(new ActionListener(){ public void actionPerformed(ActionEvent e){ skill(e); }});   
        jButton4.addActionListener(new ActionListener(){ public void actionPerformed(ActionEvent e){ run(e); }});
        fontFamily(player,1); fontFamily(playerHP,0); fontFamily(enemy,1); fontFamily(enemyHP,0); fontFamily(numHpPlayer,0); fontFamily(numHpEnemy,0);
        flatIcon(jButton1); flatIcon(jButton2); flatIcon(jButton3); flatIcon(jButton4);
        

        jPanel1.setBackground(new java.awt.Color(153,153,153));
        jPanel1.setLayout(null);
        
        jPanel2.setBackground(Color.darkGray);
        jPanel2.setLayout(null);
        jPanel2.add(text);
        text.setBounds(10, 10, 440, 120);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(10, 10, 470, 140);

        jPanel1.add(jButton1);
        jButton1.setBounds(490,10,130,60);

        jPanel1.add(jButton2);
        jButton2.setBounds(490, 90, 130,60);
       
        jPanel1.add(jButton3);
        jButton3.setBounds(640, 10, 130,60);

        jPanel1.add(jButton4);
        jButton4.setBounds(640, 90, 130,60);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 300, 800, 300);
        
        //picPlayer.setBackground(Color.white);
        picPlayer.setLayout(null);

        statusPlyer.setLayout(null);
        
        player.setHorizontalAlignment(SwingConstants.LEFT);
        player.setText("player");
        statusPlyer.add(player);
        player.setBounds(10, 10, 200, 14);

        statusPlyer.add(numHpPlayer);
        numHpPlayer.setBounds(60, 55, 100, 16);

        statusPlyer.add(HP_p);
        HP_p.setBounds(50, 31, 270, 20);

        playerHP.setText("HP :");
        statusPlyer.add(playerHP);
        playerHP.setBounds(20, 35, 34, 14);

        picPlayer.add(statusPlyer);
        statusPlyer.setBounds(20, 20, 345, 70);

        pokePlayer.setText("pokePlayer");
        picPlayer.add(pokePlayer);
        pokePlayer.setBounds(20, 100, 345, 160);

        getContentPane().add(picPlayer);
        picPlayer.setBounds(10, 10, 385, 285);

        //picEnemy.setBackground(Color.DARK_GRAY);
        picEnemy.setLayout(null);

        statusEnemy.setLayout(null);

        enemy.setHorizontalAlignment(SwingConstants.RIGHT);
        enemy.setText("enemy");
        statusEnemy.add(enemy);
        enemy.setBounds(160, 10, 150, 14);
        
        numHpEnemy.setHorizontalAlignment(SwingConstants.RIGHT);
        statusEnemy.add(numHpEnemy);
        numHpEnemy.setBounds(200, 55, 80, 16);

        
        statusEnemy.add(HP_e);
        HP_e.setBounds(20, 31, 265, 20);

        enemyHP.setHorizontalAlignment(SwingConstants.RIGHT);
        enemyHP.setText(": HP");
        statusEnemy.add(enemyHP);
        enemyHP.setBounds(280, 35, 34, 14);

        picEnemy.add(statusEnemy);
        statusEnemy.setBounds(20, 20, 330, 70);

        pokeEnemy.setText("pokeEnemy");
        picEnemy.add(pokeEnemy);
        pokeEnemy.setBounds(20, 100, 345, 160);

        getContentPane().add(picEnemy);
        picEnemy.setBounds(405, 10, 370, 280);

       
        onScreen();
        pack();

        setLocationRelativeTo(null);
        setResizable(false);
        
    }
    public void flatIcon(JButton jB) {
        jB.setBorderPainted(false);
        jB.setFocusPainted(false);
        jB.setBackground(new Color(240,240,240));
        jB.setFont(new Font("supermarket", 0, 16));
       
    }
    public void set(JButton b){
        b.setHorizontalAlignment(SwingConstants.LEFT);
        b.setHorizontalTextPosition(SwingConstants.RIGHT);
    }
    public void fontFamily(JLabel j,int type){
        j.setFont(new Font("supermarket", type, 16));
        
    }
    
    
    public void playerSetup(int E_Stat){
        num = E_Stat;
    }

    public void randMon() {
        E_Poke = pe.randName();
        E_Stat = pe.randSecond();
        E_maxHp = E_Stat[0]; 
        
    }

    public void onScreen(){
        State = "ShowPoke"; 
        Poke_bag[] pb = Poke_bag.values();
        P_Poke = pb[num];
        P_Stat = p.callPokeStat(num);
        P_maxHp = P_Stat[0];
        text.setText("\n"+ P_Poke + " stanby..");
        numHpPlayer.setText(Integer.toString(P_Stat[0]) + "/" + Integer.toString(P_maxHp));
        //numHpEnemy.setText(Integer.toString(E_Stat[0]) + "/" + Integer.toString(E_maxHP));
        HP_p.setMaximum(P_maxHp);
        HP_e.setMaximum(E_maxHp);
        HP_p.setValue(P_Stat[0]);
        HP_e.setValue(E_Stat[0]);
        player.setText(P_Poke.toString() + " : LVL " + Integer.toString(P_Stat[3]));
        enemy.setText(E_Poke.toString() + " LVL : " + Integer.toString(E_Stat[3]));
        pokePlayer.setIcon(new ImageIcon("pic/display/"+P_Poke+"_display.png"));
        pokeEnemy.setIcon(new ImageIcon("pic/display/"+E_Poke+"_display_Enemy.png"));
    }
    public void checkState(){
        if(E_Stat[0] <= 0){
            new WinBox().setVisible(true);
            this.setVisible(false);
        }
        if(P_Stat[0] <= 0){
            PokebagBox pbb = new PokebagBox();
            P_Stat[0] = 0;
            pbb.updateStat(P_Stat[0]);

            new LoseBox().setVisible(true);
            this.setVisible(false);
        }
        
    }
    private void attack(ActionEvent e){
        d.delay(1);
        damage(1);
        d.delay(1);
        damageEnemy();
        checkState();
       
    }
    private void potion(ActionEvent e){
        d.delay(1);
        damage(3);
        d.delay(1);
        damageEnemy();
    }
    private void skill(ActionEvent e){
        d.delay(1);
        damage(2);
        d.delay(1);
        damageEnemy();
        checkState();
    }
    private void run(ActionEvent e){
        d.delay(1);
        damage(4);
    }

    public void damage(int mode){
        CalAction ca = new CalAction();
        if(mode == 1){
            int damageAttack = ca.attacked(P_Stat[2], E_Stat[1]);
            if(damageAttack > 0) {
                text.setText("\n" + P_Poke + " attack "+ E_Poke );
                E_Stat[0] = E_Stat[0] - damageAttack;
                HP_e.setValue(E_Stat[0]);
            }
            else
                text.setText("\n" + P_Poke + " miss!!!");
        }
        else if(mode == 2){
            int damageSkill = ca.skill(P_Stat[2], E_Stat[1]);
                if(damageSkill > 0) {
                    text.setText("\n" + P_Poke + " skill "+ E_Poke);
                    E_Stat[0] = E_Stat[0] - damageSkill;
                    HP_e.setValue(E_Stat[0]);
                }
                else {
                    text.setText("\n" + P_Poke + " skill miss!!!");
                }            
        }
        else if(mode == 3){
            int heal = ca.heal();
            P_Stat[0] = P_Stat[0] + heal;
            if(P_Stat[0] > P_maxHp){
                P_Stat[0] = P_maxHp;
                    
            }
            numHpPlayer.setText(Integer.toString(P_Stat[0]) + "/" + Integer.toString(P_maxHp));
            text.setText("\n" + P_Poke+ " use Potion "+ heal +" hp."); 
            HP_p.setValue(P_Stat[0]);
        }
        else if(mode == 4){
        
            if(ca.escape() >= 75){
                d.delay(3);
                text.setText("\n"+ P_Poke +"RUN!!!"); 
                new RunBox().setVisible(true);
                this.setVisible(false);  
            } 
            else
                text.setText("\n" + P_Poke + " RUN Failed");       
        }
    }
    public void damageEnemy(){
        CalAction ca = new CalAction();
        int mode = (int)(Math.random()*4)+1;
        if(mode == 1){
            int damageAttack = ca.attacked(E_Stat[2], P_Stat[1]);
            if(damageAttack > 0) {
                text.setText("\n" + E_Poke + " attack "+ P_Poke );
                P_Stat[0] = P_Stat[0] - damageAttack;
                HP_p.setValue(P_Stat[0]);
                numHpPlayer.setText(Integer.toString(P_Stat[0]) + "/" + Integer.toString(P_maxHp));
            }
            else
                text.setText("\n" + P_Poke + " miss!!!");
        }
        else if(mode == 2){
            int damageSkill = ca.skill(E_Stat[2], P_Stat[1]);
                if(damageSkill > 0) {
                    text.setText("\n" + E_Poke + " skill "+ P_Poke);
                    P_Stat[0] = P_Stat[0] - damageSkill;
                    HP_p.setValue(P_Stat[0]);
                    numHpPlayer.setText(Integer.toString(P_Stat[0]) + "/" + Integer.toString(P_maxHp));
                }
                else {
                    text.setText("\n" + E_Poke + " skill miss!!!");
                }            
        }
        else if(mode == 3){
            int heal = ca.heal();
            E_Stat[0] = E_Stat[0] + heal;
            if(E_Stat[0] > E_maxHp){
                E_Stat[0] = E_maxHp;
                    
            }
            text.setText("\n" + E_Poke+ " use Potion "); 
            HP_e.setValue(E_Stat[0]);
        }
        else if(mode == 4){
            System.out.println(ca.escape());
            if(ca.escape() >= 75){
                d.delay(3);
                text.setText("\n" + E_Poke + "RUN!!!");
                new RunBox().setVisible(true);
                this.setVisible(false);  
            }
            else
                text.setText("\n" + E_Poke + " RUN Failed");         
        }
    }
    public static void main(String args[]) {
        BattleBox bb = new BattleBox();
        bb.setVisible(true);
            
    }

    private JButton jButton1;
    private JButton jButton2;
    private JButton jButton3;
    private JButton jButton4;
    
    private JLabel player;
    private JLabel enemy;
    private JLabel playerHP;
    private JLabel enemyHP;
    private JLabel pokePlayer;
    private JLabel pokeEnemy;
    private JLabel numHpPlayer;
    private JLabel numHpEnemy;


    JProgressBar HP_p;
    JProgressBar HP_e;

    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel picPlayer;
    private JPanel statusPlyer;
    private JPanel picEnemy;
    private JPanel statusEnemy;
    JTextArea text;
    Pokebag p = new Pokebag();
    Delay d = new Delay();
   // ChoicePlayer choice = new ChoicePlayer();
   // RandEnemyAction randAction = new RandEnemyAction();
    Action ac = new Action();
    Poke_bag P_Poke;
    int[] P_Stat = new int[5];
    int P_maxHp;
    Poke pe = new Poke();
    Status E_Poke;
    int[] E_Stat = new int[5];
    int E_maxHp;
    private static int num;
    String State;
}